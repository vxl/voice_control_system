import socket
import pygame
import time
import wave
import numpy as np
import speech_recognition as sr

IP = '172.20.10.2'
Port = 2002

def speech_recog(input_file):
    dat=sr.AudioFile(input_file)
    s = ""
    with dat as source:
        audio = r.record(source)
    try:
        s = r.recognize_google(audio)
        print("Text: "+s)
    except Exception as e:
        print("Exception: "+str(e))
    return s

def init_wav_file(inputfile ):
    wf = wave.open(inputfile, "wb")
    wf.setnchannels(1)
    wf.setsampwidth(2)
    wf.setframerate(44100)
    return wf

def process_text(text, server_socket):
    if('on' in text):
        data_send = 0xFFFF
        server_socket.sendall(str(data_send).encode())
    if('off' in text):
        data_send = 0x0000
        
        server_socket.sendall(str(data_send).encode())
    

def start_socket_server(host, port):
    # Tạo một đối tượng socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Liên kết đối tượng socket với địa chỉ và cổng
    server_socket.bind((host, port))

    # Lắng nghe các kết nối đến máy chủ
    wf = init_wav_file("output.wav")
    server_socket.listen(1)
    print(f"Đang lắng nghe kết nối tại {host}:{port}...")
    client_socket, client_address = server_socket.accept()
    start_time = time.time()
    while True:
        # Chấp nhận kết nối từ một máy khách
        

        # Gửi và nhận dữ liệu với máy khách
        data = client_socket.recv(1024)
        samples = np.frombuffer(data, np.int16)
        wf.writeframes(samples.tobytes())
        # Phát âm thanh dữ liệu vừa nhận được
        current_time = time.time()
        if(((current_time - start_time)>= 2) and (len(data) >100) ):
            wf.close()
            text= speech_recog("output.wav")
            print(text)
            process_text(text,client_socket)
            wf = init_wav_file("output.wav")
            start_time = current_time
    client_socket.close()
    


if __name__ == "__main__":
    # Tạo một đối tượng wave
    
    host = IP  # Địa chỉ IP của máy chủ
    port = Port         # Cổng máy chủ

    pygame.mixer.init()
    r = sr.Recognizer()
    start_socket_server(host, port)