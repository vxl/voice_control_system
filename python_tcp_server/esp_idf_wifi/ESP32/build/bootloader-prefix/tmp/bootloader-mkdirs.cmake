# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "D:/IOT/esp/Espressif/frameworks/esp-idf-v4.4.6/components/bootloader/subproject"
  "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader"
  "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader-prefix"
  "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader-prefix/tmp"
  "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader-prefix/src/bootloader-stamp"
  "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader-prefix/src"
  "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "D:/IOT/voice_control_system/python_tcp_server/esp_idf_wifi/ESP32/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
