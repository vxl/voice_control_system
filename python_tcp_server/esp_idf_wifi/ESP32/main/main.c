#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_spi_flash.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"
#include "nvs_flash.h"
#include "freertos/queue.h"
#include "tcpip_adapter.h"
#include "esp_sleep.h"
#include <driver/i2s.h>

#include "driver/gpio.h"
// Địa chỉ IP của thiết bị đã kết nối
#define DEST_IP_ADDR "172.20.10.2"
#define DEST_PORT    2002

#define STA 		 1
#define I2S_WS 25
#define I2S_SD 33
#define I2S_SCK 32
#define bufferLen 64

int16_t sBuffer[bufferLen];
// Use I2S Processor 0
#define I2S_PORT I2S_NUM_0
#define BUTTON_PIN GPIO_NUM_4
#define GPIO_IN_PIN (1ULL << BUTTON_PIN)
#define LED_PIN GPIO_NUM_2
#define TIME_TO_GO_SLEEP 1000 // sleep after 10 seconds

#ifndef STA
// SSID và mật khẩu của mạng Wi-Fi tạo bởi ESP32
#define WIFI_SSID      "ESP32_AP"
#define WIFI_PASS      "password"
#else
#define WIFI_SSID		"Hungip"
#define WIFI_PASSWORD	"88888888"

static EventGroupHandle_t wifi_event_group;
TaskFunction_t Task_send;
TaskFunction_t Task_receive;
QueueHandle_t qClick;
const int CONNECTED_BIT = BIT0;

int16_t audio_data[64]; // Sử dụng bộ đệm 64 mẫu
size_t bytes_read;
struct sockaddr_in dest_addr;
int sock;

#endif

static const char *TAG = "ESP32_TCP";
volatile uint8_t button_press = 0;
volatile uint8_t button_continue_flag = 1;
volatile uint8_t voice_flag   = 0;

void tcp_server_task(void *pvParameters);
void tcp_receive_data(void *pvParameters);


void config_led() {
    // Cấu hình GPIO cho đèn LED
    gpio_config_t io_conf = {
        .pin_bit_mask = (1ULL << LED_PIN),
        .mode = GPIO_MODE_OUTPUT,
        .intr_type = GPIO_INTR_DISABLE,
        .pull_down_en = 0,
        .pull_up_en = 0,
    };
    gpio_config(&io_conf);
    
}

static void IRAM_ATTR GPIO_INTERRUPT_HANDLER(void *arg)
{
    uint32_t gpio_num = (uint32_t)arg;
	xQueueSendFromISR(qClick, &gpio_num, NULL);
}

void config_button()
{
    gpio_config_t gpiocf = {};
    gpiocf.pin_bit_mask = GPIO_IN_PIN;
	gpiocf.mode = GPIO_MODE_INPUT;
	gpiocf.pull_up_en = 1;
	gpiocf.intr_type = GPIO_INTR_NEGEDGE;
	gpiocf.pull_down_en = 0;

	gpio_config(&gpiocf);
    gpio_install_isr_service(0);
	gpio_isr_handler_add(BUTTON_PIN,GPIO_INTERRUPT_HANDLER,(void *)BUTTON_PIN);
    printf("Config GPIO DONE \r\n");
}

void turn_on_led() {
    // Bật đèn LED
    gpio_set_level(LED_PIN, 1);
}

void turn_off_led() {
    // Tắt đèn LED
    gpio_set_level(LED_PIN, 0);
}

void Button_task(void * param){
	int gpio_num;
	TickType_t l_time = 0;
	uint32_t cnt  = 0;
    printf("Button Task\r\n");

	while(1){
		if(xQueueReceive(qClick,&gpio_num,50) ){
            printf("gpio_num %d\r\n",gpio_num);
			if(gpio_num == BUTTON_PIN && gpio_get_level(gpio_num) == 0){
                if(button_press == 0)
                {
                    button_press = 1;
                }
                button_continue_flag = 0;
				printf("Button is press!\r\n");
			}
		}
	}
}

void Task_Sleep_Monitor(void * arg)
{
    static TickType_t  tick_begin ;
    tick_begin = xTaskGetTickCount();
    esp_sleep_enable_ext0_wakeup(BUTTON_PIN,0);
    while (1)
    {
        if(button_continue_flag == 0)
        {
            button_continue_flag = 1;
            voice_flag = 1;
            printf("start listening 10s\r\n");
            tick_begin = xTaskGetTickCount();
        }
        if(xTaskGetTickCount() - tick_begin > TIME_TO_GO_SLEEP)
        {
            
            if(voice_flag == 1)
            {
                printf("stop listen!!!\r\n");
                voice_flag = 0;
            }
        }
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
}

#ifndef STA
static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
        case SYSTEM_EVENT_AP_START:
            ESP_LOGI(TAG, "Access Point started");
            break;
        case SYSTEM_EVENT_AP_STACONNECTED:
            ESP_LOGI(TAG, "Station connected");
            break;
        case SYSTEM_EVENT_AP_STADISCONNECTED:
            ESP_LOGI(TAG, "Station disconnected");
            break;
        default:
            break;
    }
    return ESP_OK;
}

void wifi_init_softap(void)
{
    wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

    esp_netif_create_default_wifi_ap();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    wifi_config_t wifi_config = {
        .ap = {
            .ssid = WIFI_SSID,
            .ssid_len = strlen(WIFI_SSID),
            .password = WIFI_PASS,
            .max_connection = 4,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}
#else
static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    switch (event_id) {
        case SYSTEM_EVENT_STA_START:
            ESP_LOGI(TAG,"WiFi connecting ... \n");
            esp_wifi_connect();
            break;
        case WIFI_EVENT_STA_CONNECTED:
            ESP_LOGI(TAG,"WiFi connected\n");
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            ESP_LOGI(TAG,"WiFi lost connection ... \n");
            esp_wifi_connect();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            break;
        default:
            break;
    }
}

esp_err_t Get_i2s_Data(uint8_t *data, int8_t * size)
{
    // Get I2S data and place in data buffer
    size_t bytesIn = 0;
    esp_err_t result = i2s_read(I2S_PORT, &sBuffer, bufferLen, &bytesIn, portMAX_DELAY);

    if (result == ESP_OK)
    {
    // Read I2S data buffer
        int16_t samples_read = bytesIn / 8; 
        if (samples_read > 0) 
        {
            *size = bytesIn;
            memcpy(data, sBuffer,bytesIn);
        }
    }
    return result;
}


void wifi_init_sta(void)
{
	wifi_event_group = xEventGroupCreate();

	tcpip_adapter_init();
	esp_event_loop_create_default();
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL);
	wifi_config_t wifi_config = {
		.sta = {
			.ssid = WIFI_SSID,
			.password = WIFI_PASSWORD,
		},
	};
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());
    esp_wifi_connect();
}
#endif

void tcp_server_task(void *pvParameters)
{
    uint8_t tx_buffer[128];
    int addr_family;
    int ip_protocol;
    esp_err_t result;
    int8_t size;
    while (1) {
        
        dest_addr.sin_addr.s_addr = inet_addr(DEST_IP_ADDR);
        dest_addr.sin_family = AF_INET;
        dest_addr.sin_port = htons(DEST_PORT);
        addr_family = AF_INET;
        ip_protocol = IPPROTO_IP;

        sock = socket(addr_family, SOCK_STREAM, ip_protocol);
        if (sock < 0) {
            ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
            break;
        }
        ESP_LOGI(TAG, "Socket created");

        int err = connect(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err != 0) {
            ESP_LOGE(TAG, "Socket unable to connect: errno %d", errno);
            ////vTaskDelete(Task_receive);
            close(sock);
            vTaskDelay(100 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "Successfully connected");
        //xTaskCreatePinnedToCore(Task_receive,tcp_receive_data, "receive data", 4096, NULL, 6, tskNO_AFFINITY);
        while (1) {  
              
            if(voice_flag == 1)
            {
                memset(tx_buffer, 0, 128);
                result = Get_i2s_Data(tx_buffer,&size);
                if((result == ESP_OK) && (size > 0))
                {
                    err = send(sock, tx_buffer, size, 0);
                    if (err < 0) 
                    {
                    ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
                    break;
                    }
                }
            }
            else 
            {
                vTaskDelay(100 / portTICK_PERIOD_MS);
            }    
            
        }

        if (sock != -1) {
            ESP_LOGE(TAG, "Shutting down socket and restarting...");
            shutdown(sock, 0);
            close(sock);
        }
    }
    vTaskDelete(NULL);
}

void tcp_receive_data(void * pvParameters)
{
    ESP_LOGI(TAG, "Receive On!");
    uint8_t rx_buffer[8];
    uint8_t buffer_on[] = {54,53,53,51,53};
    uint8_t flag = 0;

    while(1)
    {
       if(sock != NULL)
       { 
            int len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
            if (len < 0) {
                
            } else if (len == 0) {
                printf("Server disconnect\r\n");
            }
            else
            {
                // Xử lý dữ liệu nhận được
                rx_buffer[len] = '\0'; // Thêm ký tự kết thúc chuỗi
                printf("length %d\r\n", len);
                switch(len)
                {
                    case 5:
                    {
                        flag = 1;
                        for(int i = 0; i < len; i ++)
                        {
                            printf("%d\r\n",rx_buffer[i]);
                            if(rx_buffer[i] != buffer_on[i])
                            {
                                
                                flag = 0;
                            }
                            
                        }
                        if(flag == 1)
                        {
                            ESP_LOGI(TAG, "Led On!");
                            turn_on_led();
                        }
                        break;
                    }
                    case 1:
                    {
                        if(rx_buffer[0] == 48)
                        {
                            printf("%d\r\n",rx_buffer[0]);
                            ESP_LOGI(TAG, "Led Off!");
                            turn_off_led();
                            break;
                        }
                    }
                }
            }
            

            // ESP_LOGI(TAG, "Receive On!\r\n");
            // vTaskDelay(1000/portTICK_PERIOD_MS);
        }
    }
    vTaskDelete(NULL);
}

void i2s_inmp441_init()
{
    // Cấu hình I2S cho cảm biến âm thanh
    static i2s_config_t i2s_config ;
    i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX);
    i2s_config.sample_rate = 44100;
    //i2s_config.sample_rate = 11025; if you like
    i2s_config.bits_per_sample = (i2s_bits_per_sample_t)(16);
    i2s_config.channel_format = I2S_CHANNEL_FMT_ONLY_LEFT;
    //i2s_config.communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_STAND_I2S);
    i2s_config.communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB);
    i2s_config.intr_alloc_flags = 0;
    i2s_config.dma_buf_count = 8;
    i2s_config.dma_buf_len = bufferLen;
    i2s_config.use_apll = false;
    // Cấu hình GPIO cho chân MCLK


    const i2s_pin_config_t pin_config = {
    .bck_io_num = I2S_SCK,
    .ws_io_num = I2S_WS,
    .data_out_num = -1,
    .data_in_num = I2S_SD
    };

    // Thiết lập giao tiếp I2S
    i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL);
    i2s_set_pin(I2S_PORT, &pin_config);

}




void app_main(void)
{
    qClick = xQueueCreate(1,sizeof(uint32_t));

    ESP_ERROR_CHECK(nvs_flash_init());
    config_led();
    config_button();
    i2s_inmp441_init();
    i2s_start(I2S_PORT);


#ifndef STA
    ESP_LOGI(TAG, "ESP_WIFI_MODE_AP");
    wifi_init_softap();
#else
    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();
#endif
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
    ESP_LOGI(TAG, "Connected to AP");
    xTaskCreate(tcp_server_task, "tcp_server", 4096, NULL, 5, NULL);
    xTaskCreate(tcp_receive_data, "tcp_receive_data", 4096, NULL, 5, NULL);
    xTaskCreate(Button_task, "Button Task", 2048, NULL, 1, NULL);
    xTaskCreate(Task_Sleep_Monitor, "Task sleep monitor Task", 2048, NULL, 2, NULL);
    
}
